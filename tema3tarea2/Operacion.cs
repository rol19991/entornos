﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tema3tarea2
{
    /// <summary>
    /// Habilitamos la clase Operacion
    /// </summary>
    public class Operacion
    {
        #region Atributos
        public int num1 = 5;
        public int num2 = 10;
        #endregion
        /// <summary>
        /// inicializamos el metodo readonly
        /// </summary>
        public readonly int CualEsMayor;

        #region Contructor
        public Operacion(int n1, int n2)
        {
            num1 = n1;
            num2 = n2;
            if(n1 > n2)
            {
                this.CualEsMayor = n1;
            }
            else
            {
                this.CualEsMayor = n2;
            }
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Realiza la suma de dos numeros y te comunica la fecha actual.
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <returns></returns>
        public int Suma(int n1 , int n2) 
        {

            CosasFechas fechaacts = new CosasFechas(DateTime.Today);

            Console.WriteLine(fechaacts.DiaSemana(DateTime.Today));
            return n1 + n2;
            
        }
        /// <summary>
        /// Realiza la resta de dos numeros y te comunica la fecha actual.
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="CualEsMayor"></param>
        /// <returns></returns>
        public int Resta(int n1,int CualEsMayor)
        {
            CosasFechas fechaacts = new CosasFechas(DateTime.Today);

            Console.WriteLine(fechaacts.DiaSemana(DateTime.Today));
            return CualEsMayor - n1;
 
        }

        /// <summary>
        /// Realiza la multiplicacion de dos numeros y te comunica la fecha actual.
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <returns></returns>
        public int Multiplicacion(int n1, int n2)
        {
            CosasFechas fechaacts = new CosasFechas(DateTime.Today);

            Console.WriteLine(fechaacts.DiaSemana(DateTime.Today));
            return n1 * n2;
        }

        #endregion 


    }
}
